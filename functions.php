<?php
/**
 * Kvothe Kingkiller functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Kvothe_Kingkiller
 */

if ( ! function_exists( 'kvothe_kingkiller_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function kvothe_kingkiller_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on components, use a find and replace
	 * to change 'kvothe-kingkiller' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'kvothe-kingkiller', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'kvothe-kingkiller-featured-image', 640, 9999 );

	// Register menus.
	register_nav_menus( array(
            'pages' => esc_html__( 'Pages', 'kvothe-kingkiller' ),
            'authors' => esc_html__( 'Authors', 'kvothe-kingkiller' ),
            'books' => esc_html__( 'Books', 'kvothe-kingkiller' ),
        ) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'kvothe_kingkiller_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'kvothe_kingkiller_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function kvothe_kingkiller_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'kvothe_kingkiller_content_width', 640 );
}
add_action( 'after_setup_theme', 'kvothe_kingkiller_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kvothe_kingkiller_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'kvothe-kingkiller' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'kvothe_kingkiller_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function kvothe_kingkiller_scripts() {
    wp_enqueue_style( 'kvothe-kingkiller-style', get_stylesheet_uri() );

    wp_enqueue_script( 'kvothe-kingkiller-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );

    wp_enqueue_script( 'kvothe-kingkiller-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

    /* Greensock && Scrollmagic */
    wp_enqueue_script( 'gsap-js', get_template_directory_uri() . '/assets/js/TimelineMax.js', array('jquery'), false, true );
    wp_enqueue_script( 'gsap-js', get_template_directory_uri() . '/assets/js/TweenMax.js', array(), false, true );
    wp_enqueue_script( 'scrollmagic-js', get_template_directory_uri() . '/assets/js/ScrollMagic.min.js', array(), false, true );
    wp_enqueue_script( 'scrollmagic-gsap', get_template_directory_uri() . '/assets/js/animation.gsap.min.js', array(), false, true );

    /* app.js */
    wp_enqueue_script( 'app', get_template_directory_uri() . '/assets/js/app.js', array(), false, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'kvothe_kingkiller_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
