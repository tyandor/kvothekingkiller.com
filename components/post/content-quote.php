<?php
/**
 * Template part for displaying quotes.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Kvothe_Kingkiller
 */
?>

<?php $authclass = get_the_author_meta( 'nicename' ); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class($authclass); ?>>

<div class="kk-quote">
    <div class="character">
        <?php
            $character = get_the_author_meta('ID');
            $character_img = get_field('profile_pic', 'user_'. $character );
            $book = get_field( "from_the_book" );
        ?>
        <div class="likeness">
            <img src="<?php echo $character_img['url']; ?>" alt="<?php echo $character_img['alt']; ?>" />
        </div>
    </div>
    <div class="quote">
        <?php the_content(); ?>
    </div>
    <div class="name">
        <?php the_author_link(); ?> in <?php echo $book->name; ?>
    </div>
</div>

<!-- generate random post link -->
<?php
    $random_post = get_posts('posts_per_page=1&orderby=rand');
    $link = '<a href="'.get_permalink($random_post[0]->ID).'" class="button" title="Another Quote:'.get_the_title($random_post[0]->ID).'">Another Quote</a>';
?>

<div class="another-quote">
    <?php echo $link; ?>
</div>

</article>
