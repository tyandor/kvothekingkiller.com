<?php
/**
* The header for our theme.
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package Kvothe_Kingkiller
*/

?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'kvothe-kingkiller' ); ?></a>

        <div class="authors-menu">
            <?php wp_nav_menu( array( 'theme_location' => 'authors' ) ); ?>
        </div>

        <header id="masthead" class="site-header" role="banner">
            <div class="authors-and-books">
                <div id="authors">
                    <div class="svg" alt="characters"></div>
                </div>
                <div id="books">
                    <div class="svg" alt="books"></div>
                    <div class="books-menu">
                        <?php wp_nav_menu( array( 'theme_location' => 'books' ) ); ?>
                    </div>
                </div>
            </div>
            <div class="kk-logo">
                <span class="1">K</span>
                <span class="2">v</span>
                <span class="3">o</span>
                <span class="4">t</span>
                <span class="5">h</span>
                <span class="6">e</span>
                <span class="7">&nbsp;</span>
                <span class="8">K</span>
                <span class="9">i</span>
                <span class="10">n</span>
                <span class="11">g</span>
                <span class="12">k</span>
                <span class="13">i</span>
                <span class="14">l</span>
                <span class="15">l</span>
                <span class="16">e</span>
                <span class="17">r</span>
            </div>
            <div class="page-toggle-wrapper">
                <div id="pages">
                    <div class="menubar nav-one"></div>
                    <div class="menubar nav-two"></div>
                    <div class="menubar nav-three"></div>
                    <div class="menubar nav-four"></div>
                    <div class="pages-menu">
                        <?php wp_nav_menu( array( 'theme_location' => 'pages' ) ); ?>
                    </div>
                </div>
            </div>
        </header>

        <div id="content" class="site-content">
