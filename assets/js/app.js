// logo click handler
var logo = document.querySelector('.kk-logo');

logo.addEventListener('click', function (event) {
    document.location.assign('/');
});

// menu display functions
var authors = document.querySelector('#authors');
var authorsmenu = document.querySelector('.authors-menu');
var books = document.querySelector('#books');
var booksmenu = document.querySelector('.books-menu');
var pages = document.querySelector('#pages');
var pagesmenu = document.querySelector('.pages-menu');

authors.addEventListener('click', function (event) {
    if (authorsmenu.style.display == "block") {
        authorsmenu.style.display = "none";
        authors.classList.remove("active");
    } else {
        authorsmenu.style.display = "block";
        authors.classList.add("active");
        booksmenu.style.display = "none";
        books.classList.remove("active");
        pagesmenu.style.display = "none";
        pages.classList.remove("active");
    }
}
);

books.addEventListener('click', function (event) {
    if (booksmenu.style.display == "block") {
        booksmenu.style.display = "none";
        books.classList.remove("active");
    } else {
        booksmenu.style.display = "block";
        books.classList.add("active");
        authorsmenu.style.display = "none";
        authors.classList.remove("active");
        pagesmenu.style.display = "none";
        pages.classList.remove("active");
    }
}
);

pages.addEventListener('click', function (event) {
    if (pagesmenu.style.display == "block") {
        pagesmenu.style.display = "none";
        pages.classList.remove("active");
    } else {
        pagesmenu.style.display = "block";
        pages.classList.add("active");
        authorsmenu.style.display = "none";
        authors.classList.remove("active");
        booksmenu.style.display = "none";
        books.classList.remove("active");
    }
}
);


