# Kvothe Kingkiller

---

## Website

[KvotheKingkiller.com](http://kvothekingkiller.com/)

## Description

Custom design and dev built on the underscores.me template.

### Requirements

Requires at least: 4.0
Tested up to: 4.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

### Credits

* Based on Kvothe Kingkiller http://components.underscores.me/, (C) 2015-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)


