<?php
/*
 * Template Name: Home 
 *
 * @package Kvothe_Kingkiller
 *
*/
get_header(); ?>

<?php 
    $random_quote = new WP_Query('cat=2&showposts=1&orderby=rand');
    while ($random_quote->have_posts()) : $random_quote->the_post();
    $do_not_duplicate = $post->ID; 
?>

<div class="kk-quote">
    <div class="character">
        <?php
            $character = get_the_author_meta('ID');
            $character_img = get_field('profile_pic', 'user_'. $character );
            $book = get_field( "from_the_book" );
        ?>
        <div class="likeness">
            <img src="<?php echo $character_img['url']; ?>" alt="<?php echo $character_img['alt']; ?>" />
        </div>
    </div>
    <div class="quote">
        <?php the_content(); ?>
    </div>
    <div class="name">
        <?php the_author_link(); ?> in <?php echo $book->name; ?>
    </div>
</div>

<?php endwhile; ?>

<?php
    $random_post = get_posts('posts_per_page=1&orderby=rand');
    $link = '<a class="button aligncenter" href="'.get_permalink($random_post[0]->ID).'" class="button" title="Another Quote:'.get_the_title($random_post[0]->ID).'">Another Quote</a>';
?> 

<div class="another-quote">
    <?php echo $link; ?>
</div>

<?php
get_footer();
